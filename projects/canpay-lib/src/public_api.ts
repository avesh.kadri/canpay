/*
 * Public API Surface of canpay-lib
 */

export * from './lib/canpay.module';
export * from './lib/canpay-wizard/canpay-wizard.component';
export * from './lib/bancor-wc/bancor-wc.component';
export * from './lib/metamask/metamask.component';
export * from './lib/services/canpay.service';
export * from './lib/services/eth.service';
export * from './lib/services/canyacoin-eth.service';

export interface ConfirmModel {
    title: string;
    message: string;
}

export * from './canexchange/sockets/window.service';
export * from './canexchange/data/formData.service';
export * from './canexchange/workflow/workflow.service';
export * from './canexchange/details/details.service';
export * from './canexchange/result/result.service';
export * from './canexchange/complete/complete.service';
export * from './canexchange/sockets/tracker.service';
export * from './canexchange/payment/payment.service';
export * from './canexchange/sockets/tracker.component';
export * from './canexchange/staging/staging.component';
export * from './canexchange/staging/staging.service';
export * from './canexchange/details/details.component';
export * from './canexchange/payment/payment.component';
export * from './canexchange/result/result.component';
export * from './canexchange/complete/complete.component';