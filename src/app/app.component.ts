import { Component, Input, OnInit } from '@angular/core';
import { DialogService } from "ng2-bootstrap-modal";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'app';
  @Input() formData;


  constructor(private dialogService: DialogService) {
  }

  ngOnInit() {
  }
}
