import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormData } from '../data/formData.model';
import { FormDataService } from '../data/formData.service';
import { CompleteService } from '../complete/complete.service';
import { TrackerService } from '../sockets/tracker.service';
import { StagingService } from './staging.service';
import { Personal } from '../data/formData.model';
import { Observable } from 'rxjs';
import { interval } from 'rxjs';
export interface ConfirmModel {
    title: string;
    message: string;
}
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { CompleteComponent } from '../complete/complete.component';
import { ErrorComponent } from '../error/error.component'

@Component({
    selector: 'mt-wizard-result'
    , templateUrl: './staging.component.html',
    styleUrls: ['../payment/details.component.css']
})

export class StagingComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel, OnInit {
    // title = 'Thanks for staying tuned!';
    title = 'Booyah! CAN sent.';
    titleSecond = 'Your receipt has been emailed. ';
    @Input() formData: FormData;
    isFormValid: boolean = false;
    etherUrl: string;
    personal: Personal;
    activities: any[] = [];
    message: string;
    orderUrl: string;

    constructor(dialogService: DialogService, private router: Router, private formDataService: FormDataService, private completeService: CompleteService, private trackerService: TrackerService, private stagingService: StagingService) {
        super(dialogService);
        var subscription = interval(200 * 60).subscribe(x => {
           
            try {
                this.stagingService.checkStatus(this.formData.key).subscribe(activity => {
                    if (activity.status == 'COMPLETE') {
                        this.close();
                        subscription.unsubscribe();
                        let disposable = this.dialogService.addDialog(CompleteComponent, {
                            title: 'Confirm title',
                            message: 'Confirm message'
                        }).subscribe((isConfirmed) => {
                            this.close();
                            subscription.unsubscribe();
                        });

                    } else if (activity.status == 'ERROR') {
                        subscription.unsubscribe();
                        let disposable = this.dialogService.addDialog(ErrorComponent, {
                            title: 'Confirm title',
                            message: 'Confirm message'
                        }).subscribe((isConfirmed) => {
                            this.close();
                            subscription.unsubscribe();
                        });
                    }
                },
                    (error) => {
                    });
            } catch (e) {
            }

        });
    }

    ngOnInit() {
        this.formData = this.formDataService.getFormData();
        this.isFormValid = this.formDataService.isFormValid();
        this.etherUrl = 'https://etherscan.io/tx/' + this.formData.hash;
        this.orderUrl = 'http://staging.canexchange.io/#/order/' + this.formData.key;
        this.stagingService.sentMail(this.formData).subscribe(activity => {

        });

        this.stagingService.submitPost(this.formData).subscribe(activity => {

        });

    }

    showActivity(activity: any) {

        let existingActivity = false;
        let obj: any = JSON.parse(activity);

        this.formData.hash = obj.hash;
        this.formData.date = obj.date;
        this.formData.usd = this.personal.usd;
        if (obj.status == 'PROCESSED' && obj.currency == 'CAN') {
            let disposable = this.dialogService.addDialog(CompleteComponent, {
                title: 'Confirm title',
                message: 'Confirm message'
            }).subscribe((isConfirmed) => {
            });


        } else if (obj.status == 'PROCESSED' && obj.currency == 'ETH') {
            let disposable = this.dialogService.addDialog(StagingComponent, {
                title: 'Confirm title',
                message: 'Confirm message'
            }).subscribe((isConfirmed) => {
            });
        }

        if (!existingActivity && activity.page !== 'logout') {
            this.activities.push(activity);
        }
    }

    cancel() {
        this.router.navigate(['./']);
    }

    submit() {
        this.title = 'TX successful';
        this.formData = this.formDataService.resetFormData();
        this.isFormValid = false;
        this.router.navigate(['./']);
    }
}
