import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Personal } from '../data/formData.model';
import { FormDataService } from '../data/formData.service';
import { DetailsService } from './details.service';
import { FormData } from '../data/formData.model';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { PaymentComponents } from '../payment/payment.component'
declare var require: any;
export interface ConfirmModel {
    title: string;
    message: string;
}
const Web3 = require('web3');
let web3s = new Web3(Web3.currentProvider);
declare let window: any;

@Component({
    selector: 'mt-wizard-personal',
    templateUrl: './details.component.html',
    styleUrls: ['../payment/details.component.css']
})

export class DetailsComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel, OnInit {
    title = 'Enter details.';
    personal: Personal;
    form: any;
    @Input() formData: FormData;
    public web3: any;
    metamask: boolean = false;
    message: string;

    constructor(private router: Router, private formDataService: FormDataService, private detailsService: DetailsService, private loc: Location, dialogService: DialogService) {
        super(dialogService);
        if (typeof window.web3 !== 'undefined') {
            // Use Mist/MetaMask's provider
            this.metamask = true;
            this.web3 = new Web3(window.web3.currentProvider);
            this.web3.eth.getAccounts((err, accs) => {
                this.personal.address = accs.toString();
            });

        } else {
            console.log('No web3? You should consider trying MetaMask!');
            this.metamask = false;
            Web3.providers.HttpProvider.prototype.sendAsync = Web3.providers.HttpProvider.prototype.send;
            // fallback - use your fallback strategy (local node / hosted node + in-dapp id mgmt / fail)
            this.web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));
        }
    }

    confirm() {
        // we set dialog result as true on click on confirm button, 
        // then we can get dialog result from caller code 
        this.result = true;
        this.close();
    }

    showConfirm() { }

    ngOnInit() {
        this.personal = this.formDataService.getPersonal();
        this.formData = this.formDataService.getFormData();
        this.personal.currency = null;

    }

    validateAddress(address) {

        var trigger = address,
            regexp = new RegExp('^0x[a-fA-F0-9]{40}$'),
            test = regexp.test(trigger);

    }

    cancel() {
        this.formData.email = '';
        this.close();
        this.dialogService.removeAll();
    }

    save(form: any): boolean {
        if (!form.valid) {
            return false;
        }

        return true;
    }

    goToNext() {
        this.close();
        this.formData.usd = this.personal.usd;
        this.formDataService.setPersonal(this.personal);

        let disposable = this.dialogService.addDialog(PaymentComponents, {
            title: 'Confirm title',
            message: 'Confirm message'
        })
            .subscribe((isConfirmed) => {

            });

    }
}
