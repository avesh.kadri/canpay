import { Component, OnInit, AfterViewInit, Input, Output, EventEmitter } from '@angular/core';
import { Renderer2 } from '@angular/core';
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";
import { DetailsComponent } from '../../canexchange/details/details.component'
import { interval } from 'rxjs';
import { CanpayWizardComponent } from '../canpay-wizard/canpay-wizard.component'
import { FormData } from '../../canexchange/data/formData.model';
import { FormDataService } from '../../canexchange/data/formData.service';
import { DetailsService } from '../../canexchange/details/details.service';
import { Personal } from '../../canexchange/data/formData.model';

export enum Status {
  New,
  PendingPurchase,
  InProgress,
  Completed
}

@Component({
  selector: 'canyalib-bancor-wc',
  templateUrl: './bancor-wc.component.html',
  styleUrls: ['./bancor-wc.component.css']
})
export class BancorWcComponent implements OnInit, AfterViewInit {
  @Output() check = new EventEmitter();
  @Input() type = 'WITHOUT_INPUT_BOXES';
  @Input() balance = 0;
  @Input() set isLoading(isLoading: boolean) {
    if (!isLoading) {
      this.status = Status.New;
    }
  }
  @Input() formData: FormData;
  currentBalance: Number;
  Status = Status;
  status: Status = Status.New;
  personal: Personal;

  constructor(private renderer: Renderer2, private detailsService: DetailsService, private dialogService: DialogService, private canpayWizardComponent: CanpayWizardComponent, private formDataService: FormDataService) { }

  ngOnInit() {
    this.formData = this.formDataService.getFormData();
    this.personal = this.formDataService.getPersonal();
    var subscription = interval(50 * 60).subscribe(x => {
      this.canpayWizardComponent.checkBalanceAfterCredit(this.formData.address, this.balance);
    });

  }


  ngAfterViewInit() { }

  addJsToElement(src: string): HTMLScriptElement {
    const script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = src;
    this.renderer.appendChild(document.body, script);
    return script;
  }

  open() {
    this.status = Status.PendingPurchase;
  }

  checkBalance() {
    console.log('in checkBalance');
    this.status = Status.InProgress;
    setTimeout(() => this.check.emit(), 2000);
  }



  public callCanExe(balance) {

    this.formData.amount = +localStorage.getItem('oldamount') - balance;
     
    this.formData.usd = this.formData.amount * +localStorage.getItem('usd');
    this.dialogService.addDialog(DetailsComponent, {
      title: 'Confirm title',
      message: 'Confirm message'
    })
      .subscribe((isConfirmed) => {

      });
  }

  cancel() {
    this.status = Status.New;
  }

}
