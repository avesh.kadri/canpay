'use strict';
var HDWalletProvider = require("truffle-hdwallet-provider");

module.exports = {
  networks: {
    ropsten: {
      network_id: 3,
      gas: 2000000,
      gasPrice: 2000000,
      provider: function () {
        return new HDWalletProvider('supply approve laptop thumb tackle aspect jump kangaroo secret donkey firm present', 'https://ropsten.infura.io/v3/456e69d0367b45469143776e75ddcb0a')
      }
    }
  }
};