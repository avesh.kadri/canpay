import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ResultService } from '../result/result.service';
import { FormData } from '../data/formData.model';
import { FormDataService } from '../data/formData.service';
import { PaymentService } from '../payment/payment.service';
import { ResizeService } from '../../lib/services/resize.service';
import { Subscription } from 'rxjs';
export interface ConfirmModel {
    title: string;
    message: string;
}
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { ResultComponent } from '../result/result.component';
import { DetailsComponent } from '../details/details.component';
import { PaymentComponentERC } from '../payment-erc20/payment-erc20.component';


@Component({
    selector: 'mt-wizard-work'
    , templateUrl: './payment.component.html',
    styleUrls: ['./details.component.css']
})

export class PaymentComponents extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel, OnInit {
    title = 'Please Review and Confirm Your Transaction!';
    workType: boolean;
    form: any;
    @Input() formData: FormData;
    isFormValid: boolean = false;
    can: boolean = false;
    etherium: boolean = false;
    etheriumAddress: string = '0xF0725197ca2C41e61912d97C91FebCeE21664F65';
    etherPrise: number;
    key: any;
    status: any;
    error: any;
    tokens: any = [];
    others: boolean = false;
    otherstest: any;
    message: string;
    changeButtonToSelectCurrency: boolean = false;
    erc20: boolean = false;
    validData: boolean = false;
    token_classes: string = '';
    private resizeSubscription: Subscription;

    constructor(private router: Router, private resizeService: ResizeService, private formDataService: FormDataService, private paymentService: PaymentService, private resultService: ResultService, dialogService: DialogService) {
        super(dialogService);
    }

    ngOnInit() {
        this.workType = this.formDataService.getConfirmation();
        this.formData = this.formDataService.getFormData();
        this.isFormValid = this.formDataService.isFormValid();

        this.paymentService.getTokens().subscribe(data => {
            for (let result of data) {
                this.tokens.push(result);
            }
        });

        this.paymentService.getSessionId().subscribe(data => {
            this.key = data.token;
            this.status = data.status;
        });

        if (window.innerWidth < 769) {
            this.token_classes = 'card-holder col-xs-6 payment-margin-right';
        } else {
            this.token_classes = 'card-holder col-xs-4 payment-margin-right';
        }

        this.resizeSubscription = this.resizeService.onResize$.subscribe(size => {
            if (size.innerWidth < 769) {
                this.token_classes = 'card-holder col-xs-6 payment-margin-right';
            } else {
                this.token_classes = 'card-holder col-xs-4 payment-margin-right';
            }
        });
    }

    ngOnDestroy() {
        if (this.resizeSubscription) {
            this.resizeSubscription.unsubscribe();
        }
    }

    selectCurrency(currency) {
        this.otherstest = currency;
        this.formData.currency = currency;

        if (currency == 'ETH') {
            this.changeButtonToSelectCurrency = false;
            this.erc20 = false;
            this.can = false;
            this.etherium = !this.etherium;
            this.others = false;
            this.paymentService.getData(currency).subscribe(
                (data) => {
                    let price = data.data.price * +this.formData.amount;
                    this.formData.eth = +price.toFixed(6);
                    this.etherPrise = +price.toFixed(6);
                    this.validData = true;
                }
            );

            setTimeout(() => {
                if (this.status && this.formData.currency != null) {
                    this.error = null;
                    this.formData.key = this.key;
                    // Navigate to the result page
                    this.formData.accept = true;

                    this.formDataService.setConfirmation(this.workType);

                    this.close();
                    let disposable = this.dialogService.addDialog(ResultComponent, {
                        title: 'Confirm title',
                        message: 'Confirm message'
                    })
                        .subscribe((isConfirmed) => {

                        });
                } else {
                    this.error = 'Oops! something went wrong, Please try again later.';
                }
            }, 1000);



        } else if (currency == 'erc20') {
            this.changeButtonToSelectCurrency = true;
            this.etherium = false;
            this.erc20 = !this.erc20;
            this.others = false;
            setTimeout(() => {
                this.close();
                let disposable = this.dialogService.addDialog(PaymentComponentERC, {
                    title: 'Confirm title',
                    message: 'Confirm message'
                })
                    .subscribe((isConfirmed) => {

                    });

            }, 500);

        } else {
            this.changeButtonToSelectCurrency = false;
            this.erc20 = false;
            this.etherium = false;
            this.can = false;
            this.others = !this.others;
            this.paymentService.getData(currency).subscribe(
                (data) => {
                    let price = data.data.price * +this.formData.amount;
                    this.formData.eth = +price.toFixed(6);
                    this.etherPrise = +price.toFixed(6);
                }
            );
        }


    }

    save(form: any): boolean {
        if (!form.valid) {
            return false;
        }

        this.formDataService.setConfirmation(this.workType);
        return true;
    }

    goToPrevious(form: any) {
        if (this.save(form)) {
            this.close();

            let disposable = this.dialogService.addDialog(DetailsComponent, {
                title: 'Confirm title',
                message: 'Confirm message'
            })
                .subscribe((isConfirmed) => {

                });
        }
    }

    cancel() {
        this.formData.email = '';
        this.close();
        this.dialogService.removeAll();
    }

    goToNext(form: any, key: any) {
        if (this.save(form) && this.status && this.formData.currency != null) {
            this.error = null;
            this.formData.key = key;
            // Navigate to the result page
            this.formData.accept = true;

            this.close();
            let disposable = this.dialogService.addDialog(ResultComponent, {
                title: 'Confirm title',
                message: 'Confirm message'
            })
                .subscribe((isConfirmed) => {

                });
        } else {
            this.error = 'Oops! something went wrong, Please try again later.';
        }
    }
}