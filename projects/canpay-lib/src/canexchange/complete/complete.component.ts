import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormData } from '../data/formData.model';
import { FormDataService } from '../data/formData.service';
import { CompleteService } from '../complete/complete.service';
import * as globals from '../globals';
export interface ConfirmModel {
    title: string;
    message: string;
}
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";

@Component({
    selector: 'mt-wizard-result'
    , templateUrl: './complete.component.html',
    styleUrls: ['../payment/details.component.css']
})

export class CompleteComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel, OnInit {
    // title = 'Thanks for staying tuned!';
    title = 'Booyah! CAN sent.';
    titleSecond = 'Your receipt has been emailed. ';
    @Input() formData: FormData;
    isFormValid: boolean = false;
    etherUrl: string;
    message: string;
    orderUrl: string;

    constructor(dialogService: DialogService, private router: Router, private formDataService: FormDataService, private completeService: CompleteService) {
        super(dialogService);

    }


    ngOnInit() {

        this.formData = this.formDataService.getFormData();
        this.isFormValid = this.formDataService.isFormValid();
        this.etherUrl = globals.etherscan + this.formData.hash;

        this.orderUrl = 'http://staging.canexchange.io/#/order/' + this.formData.key;
        try {

            this.completeService.checkStatus(this.formData.key).subscribe(activity => {
                this.formData.hash = activity.hashEthertoAccount;
            },
                (error) => {

                });


        }
        catch (e) {

        }
        this.completeService.sentMail(this.formData).subscribe(activity => {

        });

    }

    cancel() {
        this.formDataService.resetFormData();
        this.close();
        this.dialogService.removeAll();
    }

    submit() {
        this.title = 'TX successful';
        this.formData = this.formDataService.resetFormData();
        this.isFormValid = false;
        this.router.navigate(['./']);
    }
}
