// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: true,
  contracts: {
    canYaDao: '0x17b4ae55a5b0b6c10b0f4bae2d75a4e83de41709',
    canYaCoin: '0x1d462414fe14cf489c7a21cac78509f4bf8cd7c0',
    useTestNet: false,
    testAccount: '0xf0725197ca2c41e61912d97c91febcee21664f65'
  },
  ethNetId: 1524196056249
};
