import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Observable, Observer, Subscription } from 'rxjs';
import { WindowRef } from './window.service';
import * as SockJS from 'sockjs-client';
import * as Stomp from 'webstomp-client';
import { Location } from '@angular/common';
import * as globals from '../globals';

@Injectable()
export class TrackerService {
    stompClient = null;
    subscriber = null;
    connection: Promise<any>;
    connectedPromise: any;
    listener: Observable<any>;
    listenerObserver: Observer<any>;
    alreadyConnectedOnce = false;
    private subscription: Subscription;
    private lock: string;

    constructor(
        private router: Router,
        private $window: WindowRef, private loc: Location
    ) {
        this.connection = this.createConnection();
        this.listener = this.createListener();
    }

    getHostUrl() {
        var url = window.location.href.replace('4200', '8080');
        var subUrl = url.replace('/result', '');
        return subUrl.replace("http:", '');
    }

    connect(formData) {
        this.lock = formData.lock;
        if (this.connectedPromise === null) {
            this.connection = this.createConnection();
        }
        // building absolute path so that websocket doesn't fail when deploying with a context path
        const loc = this.$window.nativeWindow.location;
        let url;

        url = globals.socketsDev + globals.contextPath + 'websocket/tracker';
        const authToken = 'test';
        if (authToken) {
            url += '?access_token=' + authToken;
        }
        const socket = new SockJS(url);
        this.stompClient = Stomp.over(socket);
        const headers = {};
        this.stompClient.connect(
            headers,
            () => {
                this.connectedPromise('success');
                this.connectedPromise = null;
                formData.key = this.lock;
                this.sendActivity(formData);

            }
        );


    }

    connectStaging(formData) {
        this.lock = formData.lock;
        if (this.connectedPromise === null) {
            this.connection = this.createConnection();
        }
        // building absolute path so that websocket doesn't fail when deploying with a context path
        const loc = this.$window.nativeWindow.location;
        let url;

        url = globals.socketsDev + globals.contextPath + '/websocket/tracker';

        const authToken = 'test';
        if (authToken) {
            url += '?access_token=' + authToken;
        }
        const socket = new SockJS(url);
        this.stompClient = Stomp.over(socket);
        const headers = {};
        this.stompClient.connect(
            headers,
            () => {
                this.connectedPromise('success');
                this.connectedPromise = null;
                formData.key = this.lock;
                this.sendStagingActivity(formData);

            }
        );
    }

    disconnect() {
        if (this.stompClient !== null) {
            this.stompClient.disconnect();
            this.stompClient = null;
        }
        if (this.subscription) {
            this.subscription.unsubscribe();
            this.subscription = null;
        }
        this.alreadyConnectedOnce = false;
    }

    receive() {
        return this.listener;
    }

    sendActivity(formData) {
        if (this.stompClient !== null && this.stompClient.connected) {
            this.stompClient.send(
                '/topic/transaction/' + this.lock, // destination
                JSON.stringify(formData), // body
                { key: { 'key': 'test' } } // header
            );
        }
    }

    sendStagingActivity(formData) {
        if (this.stompClient !== null && this.stompClient.connected) {

            this.stompClient.send(
                '/topic/transaction/staging/' + this.lock, // destination
                JSON.stringify({ formData }), // body
                { key: { 'key': 'test' } } // header
            );
        }
    }


    subscribe(key) {

        this.lock = key;
        this.connection.then(() => {

            this.subscriber = this.stompClient.subscribe('/topic/transaction/' + key, data => {
                this.listenerObserver.next(data.body);
            });
        });
    }

    subscribeStaging(key) {
        this.lock = key;
        this.connection.then(() => {

            this.subscriber = this.stompClient.subscribe('/topic/transaction/staging/' + key, data => {
                this.listenerObserver.next(data.body);
            });
        });
    }

    unsubscribe() {
        if (this.subscriber !== null) {
            this.subscriber.unsubscribe();
        }
        this.listener = this.createListener();
    }

    private createListener(): Observable<any> {
        return new Observable(observer => {
            this.listenerObserver = observer;
        });
    }

    private createConnection(): Promise<any> {
        return new Promise((resolve, reject) => (this.connectedPromise = resolve));
    }
}
