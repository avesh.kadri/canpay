export const environment = {
  production: true,
  contracts: {
    canYaDao: '0x17b4ae55a5b0b6c10b0f4bae2d75a4e83de41709',
    canYaCoin: '0x1b04ae0cbc58f813373ce2a854b97785762bdd34',
    useTestNet: true,
    testAccount: '0xf0725197ca2c41e61912d97c91febcee21664f65'
  },
  ethNetId: 1524196056249
};
