import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BootstrapModalModule } from 'ng2-bootstrap-modal';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { CanpayWizardComponent } from './canpay-wizard/canpay-wizard.component';
import { MetamaskComponent } from './metamask/metamask.component';
import { EthService } from './services/eth.service';
import { FaqComponent } from './metamask/faq/faq.component';
import { InstructionsComponent } from './metamask/instructions/instructions.component';
import { LoadingStatusComponent } from './loading-status/loading-status.component';
import { BancorWcComponent } from './bancor-wc/bancor-wc.component';
import { CommaSepNumPipe } from './comma-sep-num.pipe';
import { PaymentAuthorisationComponent } from './payment-authorisation/payment-authorisation.component';
import { PaymentComponent } from './payment/payment.component';
import { MsgBoxComponent } from './msg-box/msg-box.component';
import { ProcessComponent } from './process/process.component';
import { CanpayModalComponent } from './canpay-modal/canpay-modal.component';
import { InputAmountComponent } from './input-amount/input-amount.component';
import { CanPayService } from './services/canpay.service';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { CanYaCoinEthService } from './services/canyacoin-eth.service';

/* Shared Service */
import { WindowRef } from '../canexchange/sockets/window.service';
import { FormDataService } from '../canexchange/data/formData.service';
import { WorkflowService } from '../canexchange/workflow/workflow.service';
import { DetailsService } from '../canexchange/details/details.service';
import { ResultService } from '../canexchange/result/result.service';
import { CompleteService } from '../canexchange/complete/complete.service';
import { TrackerService } from '../canexchange/sockets/tracker.service';
import { PaymentService } from '../canexchange/payment/payment.service';
import { TrackerComponent } from '../canexchange/sockets/tracker.component';
import { QRCodeModule } from 'angular2-qrcode';
import { ClipboardModule } from 'ngx-clipboard';
import { StagingComponent } from '../canexchange/staging/staging.component';
import { StagingService } from '../canexchange/staging/staging.service';
import { DatePipe } from '@angular/common';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { DetailsComponent } from '../canexchange/details/details.component';
import { PaymentComponents } from '../canexchange/payment/payment.component';
import { ResultComponent } from '../canexchange/result/result.component';
import { CompleteComponent } from '../canexchange/complete/complete.component';
import { OrderComponent } from '../canexchange/order/order.component';
import { ErrorComponent } from '../canexchange/error/error.component';
import { OrderService } from '../canexchange/order/order.service';
import { PaymentServiceERC } from '../canexchange/payment-erc20/payment-erc20.service';
import { PaymentComponentERC } from '../canexchange/payment-erc20/payment-erc20.component';
import { ResizeService } from '../lib/services/resize.service';

const COMPONENTS = [
  CanpayModalComponent,
  CanpayWizardComponent,
  MetamaskComponent,
  FaqComponent,
  InstructionsComponent,
  LoadingStatusComponent,
  BancorWcComponent,
  PaymentAuthorisationComponent,
  PaymentComponent,
  ProcessComponent,
  MsgBoxComponent,
  InputAmountComponent,
  CommaSepNumPipe
];

const PROVIDERS = [EthService, CanYaCoinEthService, CanPayService];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    BootstrapModalModule.forRoot({ container: document.body }),
    ClipboardModule,
    QRCodeModule
  ],
  entryComponents: [
    CanpayModalComponent,
    ErrorComponent,
    TrackerComponent, DetailsComponent, ResultComponent, PaymentComponents, CompleteComponent, StagingComponent, OrderComponent, PaymentComponentERC
  ],
  declarations: [
    COMPONENTS,
    ErrorComponent,
    TrackerComponent, DetailsComponent, ResultComponent, PaymentComponents, CompleteComponent, StagingComponent, OrderComponent, PaymentComponentERC
  ],
  exports: COMPONENTS,
  providers: [
    PROVIDERS,
    StagingService, PaymentServiceERC, FormDataService, WorkflowService, DetailsService, TrackerService,
    ResultService, CompleteService, PaymentService, HttpClientModule, WindowRef, OrderService, ResizeService
  ]
})
export class CanpayModule {
  static forRoot(config: any): ModuleWithProviders {
    return {
      ngModule: CanpayModule,
      providers: [{ provide: 'Config', useValue: config }]
    };
  }
}
