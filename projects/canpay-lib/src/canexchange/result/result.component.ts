import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { FormData } from '../data/formData.model';
import { FormDataService } from '../data/formData.service';
import { ResultService } from '../result/result.service';
import { Personal } from '../data/formData.model';
import { Http, Response } from '@angular/http';
import * as globals from '../globals';
import { interval } from 'rxjs';

declare var require: any;
const Web3 = require('web3');
declare var web3;
let web3s = new Web3(Web3.currentProvider);
declare let window: any;
const gasStationApi = 'https://ethgasstation.info/json/ethgasAPI.json';

export interface ConfirmModel {
    title: string;
    message: string;
}
import { DialogComponent, DialogService } from 'ng2-bootstrap-modal';
import { CompleteComponent } from '../complete/complete.component';
import { StagingComponent } from '../staging/staging.component';
import { ErrorComponent } from '../error/error.component';

@Component({
    selector: 'mt-wizard-result'
    , templateUrl: './result.component.html',
    styleUrls: ['../payment/details.component.css']
})

export class ResultComponent extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel, OnInit {
    // title = 'Thanks for staying tuned!';
    title = 'Pay exactly';
    @Input() formData: FormData;
    isFormValid: boolean = false;
    activities: any[] = [];
    dataSuccess: any;
    personal: Personal;
    copied: boolean;
    ethStatus: boolean = false;
    status: string;
    public web3: any;
    etheriumAddress: string = globals.etheriumAddress;
    metamaskpayment: boolean = false;
    tokenABI: any;
    public MyContract: any;
    web3js: any;
    canyaContract: any;
    message: string;
    orderUrl: string;
    dai: boolean = false;

    constructor(dialogService: DialogService, protected http: Http, private resultService: ResultService, private router: Router, private formDataService: FormDataService, private route: Router) {
        super(dialogService);
        this.tokenABI = [{ "constant": true, "inputs": [], "name": "name", "outputs": [{ "name": "", "type": "string" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_spender", "type": "address" }, { "name": "_value", "type": "uint256" }], "name": "approve", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [], "name": "totalSupply", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_from", "type": "address" }, { "name": "_to", "type": "address" }, { "name": "_value", "type": "uint256" }], "name": "transferFrom", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [{ "name": "", "type": "address" }], "name": "balances", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "decimals", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "", "type": "address" }, { "name": "", "type": "address" }], "name": "allowed", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [{ "name": "_owner", "type": "address" }], "name": "balanceOf", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "totalTokens", "outputs": [{ "name": "", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": true, "inputs": [], "name": "symbol", "outputs": [{ "name": "", "type": "string" }], "payable": false, "stateMutability": "view", "type": "function" }, { "constant": false, "inputs": [{ "name": "_to", "type": "address" }, { "name": "_value", "type": "uint256" }], "name": "transfer", "outputs": [{ "name": "", "type": "bool" }], "payable": false, "stateMutability": "nonpayable", "type": "function" }, { "constant": true, "inputs": [{ "name": "_owner", "type": "address" }, { "name": "_spender", "type": "address" }], "name": "allowance", "outputs": [{ "name": "remaining", "type": "uint256" }], "payable": false, "stateMutability": "view", "type": "function" }, { "inputs": [], "payable": false, "stateMutability": "nonpayable", "type": "constructor" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "from", "type": "address" }, { "indexed": true, "name": "to", "type": "address" }, { "indexed": false, "name": "value", "type": "uint256" }], "name": "Transfer", "type": "event" }, { "anonymous": false, "inputs": [{ "indexed": true, "name": "owner", "type": "address" }, { "indexed": true, "name": "spender", "type": "address" }, { "indexed": false, "name": "value", "type": "uint256" }], "name": "Approval", "type": "event" }];


        try {
            let subscription = interval(200 * 60).subscribe(x => {

                this.resultService.checkStatus(this.formData.key).subscribe(activity => {
                    if (activity.status == 'IDENTIFIED') {
                        subscription.unsubscribe();

                        this.close();
                        let disposable = this.dialogService.addDialog(StagingComponent, {
                            title: 'Confirm title',
                            message: 'Confirm message'
                        }).subscribe((isConfirmed) => {

                        });

                    } else if (activity.status == 'ERROR') {
                        subscription.unsubscribe();
                        this.close();
                        let disposable = this.dialogService.addDialog(ErrorComponent, {
                            title: 'Confirm title',
                            message: 'Confirm message'
                        }).subscribe((isConfirmed) => {

                        });
                    }
                },
                    (error) => {

                    });


            });
        } catch (e) {

        }
    }

    createContractInstance(abi, address) {
        if (!this.web3js) {
            console.log('Error createContractInstance, web3 provider not initialized');
            return;
        }

        return new this.web3js.eth.Contract(abi, address);
    }

    getDefaultGasPriceGwei(): Promise<string> {
        return new Promise((resolve, reject) => {
            try {
                this.http.get(globals.rootUrlDev + globals.contextPath + 'api/getGas').toPromise().then(res => {
                    if (res.ok) {
                        resolve(JSON.parse(res.text())['fast'].toString() + '00000000');
                    } else {
                        resolve('11000000000');
                    }
                });
            } catch (e) {
                resolve('11000000000');
            }
        });
    }


    amountToCANTokens(amount) {
        return amount * Math.pow(10, this.formData.erc20tokenDecimal);
    }

    submit() {
        alert('Done!');
        this.title = 'TX successful';
        this.formData = this.formDataService.resetFormData();
        this.isFormValid = false;
    }


    copyToClipboard(element) {
        this.copied = true;

        setTimeout(() => {
            this.copied = false;
        }, 1000);
    }


    metamask() {

        if (typeof window.web3 === 'undefined') {
            alert('You need to install MetaMask to use this feature.  https://metamask.io');
        }
        this.web3 = new Web3(window.web3.currentProvider);

        this.web3.eth.getAccounts((err, accs) => {

            let user_address = accs.toString();

            if (this.formData.currency == 'ETH') {
                window.web3.eth.sendTransaction({
                    to: globals.etheriumAddress,
                    from: user_address,
                    value: window.web3.toWei(this.formData.eth, 'ether'),
                }, function (err, transactionHash) {
                    if (err) return alert('Oh no!: ' + err.message)

                })
            } else {
                this.web3js = new Web3(web3.currentProvider);
                this.canyaContract = this.createContractInstance(this.tokenABI, this.formData.erc20token);

                const MyContract = web3.eth.contract(this.tokenABI);
                const myContractInstance = MyContract.at(this.formData.erc20token);

                this.resultService.getGasPrice().subscribe(activity => {

                    myContractInstance.transfer(globals.etheriumAddress, this.amountToCANTokens(this.formData.eth),
                        { from: user_address, gas: activity.fast + '000' });

                });

            }

        });

    }

    metamaskEnable() {
        this.metamaskpayment = true;
    }

    showActivity(activity: any) {

        let existingActivity = false;
        let obj: any = JSON.parse(activity);

        this.formData.hash = obj.hash;
        this.formData.date = obj.date;
        this.formData.usd = this.personal.usd;

        if (obj.status === 'PROCESSED' && obj.currency === 'CAN') {
            this.close();
            let disposable = this.dialogService.addDialog(CompleteComponent, {
                title: 'Confirm title',
                message: 'Confirm message'
            }).subscribe((isConfirmed) => {

            });


        } else if (obj.status === 'PROCESSED' && obj.currency === 'ETH') {
            this.close();
            let disposable = this.dialogService.addDialog(StagingComponent, {
                title: 'Confirm title',
                message: 'Confirm message'
            }).subscribe((isConfirmed) => {

            });

        } else if (obj.status === 'PROCESSED' && obj.currency === 'METAMASK') {
            this.close();
            let disposable = this.dialogService.addDialog(CompleteComponent, {
                title: 'Confirm title',
                message: 'Confirm message'
            }).subscribe((isConfirmed) => {

            });

        }

        if (!existingActivity && activity.page !== 'logout') {
            this.activities.push(activity);
        }
    }


    cancel() {
        this.formData.email = '';
        this.close();
        this.dialogService.removeAll();
    }

    ngOnInit() {

        this.copied = false;
        this.formData = this.formDataService.getFormData();
        this.orderUrl = 'http://staging.canexchange.io/#/order/' + this.formData.key;
        if (this.formData.currency == 'ETH') {
            this.ethStatus = true;
        }

        if (this.formData.currency === 'Dai') {
            this.dai = true;
        }

        this.isFormValid = this.formDataService.isFormValid();
        this.personal = this.formDataService.getPersonal();

        this.resultService.save(this.formData).subscribe(activity => {
        });

    }

}
