import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormData } from '../../canexchange/data/formData.model';
import { FormDataService } from '../../canexchange/data/formData.service';
import { Personal } from '../../canexchange/data/formData.model';
import { DetailsService } from '../../canexchange/details/details.service';

@Component({
  selector: 'canyalib-input-amount',
  templateUrl: './input-amount.component.html',
  styleUrls: ['./input-amount.component.scss']
})
export class InputAmountComponent {
  @Output() amountUpdate = new EventEmitter();
  @Output() error = new EventEmitter();
  @Input() minAmount = 1;
  @Input() maxAmount = 0;
  @Input() formData: FormData;
  personal: Personal;
  totalPrice: string;

  amount: number;

  constructor(private formDataService: FormDataService, private detailsService: DetailsService) {
    this.formData = this.formDataService.getFormData();
    this.personal = this.formDataService.getPersonal();
  }

  onAmountKeyUp(event) {
    localStorage.setItem('oldamount', '');
    this.amount = Number(event.target.value);
    localStorage.setItem('oldamount', this.amount + '');
  }

  setAmount() {
    const amount = Number(this.amount);
    this.formData.usd = amount;

    this.formData.amount = +amount;

    this.detailsService.getData('CAN').subscribe(
      (data) => {
        const price = (this.amount * data.data.quotes.USD.price).toFixed(6);
        this.formData.eth = +price;
        this.formData.usd = +price;
        this.totalPrice = price;
        localStorage.setItem('usd', data.data.quotes.USD.price);

        this.personal.usd = +price;
      }
    );

    if (isNaN(amount) || amount < this.minAmount || (this.maxAmount && amount > this.maxAmount)) {
      const minAmountMsg = this.minAmount ? ', min allowed amount is ' + this.minAmount + ' CAN' : '';
      const maxAmountMsg = this.maxAmount ? ', max allowed amount is ' + this.maxAmount + ' CAN.' : '.';
      this.error.emit('Invalid payment amount' + minAmountMsg);
      return;
    }

    this.error.emit();
    this.amountUpdate.emit(amount);
  }
}
