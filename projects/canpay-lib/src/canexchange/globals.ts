'use strict';

export const sep='/';
export const version: string="22.2.2";  
export const bancor: string = "https://api.bancor.network/0.1/currencies/CAN/ticker?fromCurrencyCode=";
export const cmc: string = "https://api.coinmarketcap.com/v2/ticker/2343/?convert=";
export const etherscan = "https://etherscan.io/tx/";

export const socketsDev: string = "//staging.canexchange.io";
export const rootUrlDev: string = "http://staging.canexchange.io";
export const contextPath: string = "/backend/"

// export const socketsDev: string = "//localhost:8080";
// export const rootUrlDev: string = "http://localhost:8080";
// export const contextPath: string = "/"
export const etheriumAddress: string = "0xf0725197ca2c41e61912d97c91febcee21664f65"; 
export const contractOwner: string = "0xee9154ab6366416e80a1eb718954abe2ae406274";