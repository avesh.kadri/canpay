import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { Personal } from '../data/formData.model';
import { FormDataService } from '../data/formData.service';
import { FormData } from '../data/formData.model';
export interface ConfirmModel {
    title: string;
    message: string;
}
import { DialogComponent, DialogService } from "ng2-bootstrap-modal";

@Component({
    selector: 'mt-wizard-personal',
    templateUrl: './error.component.html',
    styleUrls: ['../payment/details.component.css']
})

export class ErrorComponent  extends DialogComponent<ConfirmModel, boolean> implements ConfirmModel, OnInit  {
    title = 'Enter details.';
    personal: Personal;
    form: any;
    @Input() formData: FormData;
    public web3: any;
    metamask: boolean = false;
    message: string;
    
    constructor(dialogService: DialogService, private router: Router, private formDataService: FormDataService ) {
        super(dialogService);
    }

    ngOnInit() {
        this.personal = this.formDataService.getPersonal();
        this.formData = this.formDataService.getFormData();
        this.personal.currency = null;
    }

    cancel() {
        this.router.navigate(['./']);
    }

}
