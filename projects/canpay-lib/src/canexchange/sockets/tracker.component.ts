import { Component, OnInit, OnDestroy } from '@angular/core';
import {Router} from '@angular/router';
import { TrackerService } from './tracker.service';

@Component({
    selector: 'tracker',
    templateUrl: './tracker.component.html',
    styleUrls: ['../payment/details.component.css']
})
export class TrackerComponent implements OnInit, OnDestroy {
    activities: any[] = [];

    constructor(private trackerService: TrackerService, private route: Router) {
    }

    goToNext() {
        this.route.navigateByUrl('/details');
    }

    showActivity(activity: any) {
        let existingActivity = false;
        
        for (let index = 0; index < this.activities.length; index++) {
           
            if(this.activities[index].userLogin === 'admin') {
                this.route.navigateByUrl('/complete');
            }
            if (this.activities[index].sessionId === activity.sessionId) {
                
                existingActivity = true;
                if (activity.page === 'logout') {
                    this.activities.splice(index, 1);
                } else {
                    this.activities[index] = activity;
                }
            }
        }
        if (!existingActivity && activity.page !== 'logout') {
            this.activities.push(activity);
        }
    }

    ngOnInit() { 
         
        this.trackerService.receive().subscribe(activity => {
            this.showActivity(activity);
        } );
    }

    ngOnDestroy() {
        this.trackerService.unsubscribe();
    }
}
